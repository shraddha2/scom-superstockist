package com.scom5g.superstockists;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.scom5g.superstockists.Receiver.ConnectivityReceiver;

/**
 * Created by Sanket on 27-01-2018.
 */

public class Scom5GSuperStockists extends Application {

    private static Scom5GSuperStockists mInstance;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public static synchronized Scom5GSuperStockists getInstance() {
        return mInstance;


    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


}

