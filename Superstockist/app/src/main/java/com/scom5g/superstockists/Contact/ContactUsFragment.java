package com.scom5g.superstockists.Contact;

/*Edited By: Shraddha Shetkar
  Date: 17/04/2018
  Des: Display the contact details of the Scom Organization */

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.scom5g.superstockists.R;

public class ContactUsFragment extends android.app.Fragment {


    public ContactUsFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }
}
